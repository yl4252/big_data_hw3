#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2019-02
"""

# %% Packages
import numpy as np
import pandas as pd


# %% Main Function
def create_crsp_calendar(db):
    """"
    Create CRSP daily calender.
    Input:
        db: WRDS connection
    Return:
        tdays: pandas costume business days calendar
    """

    #db = wrds.Connection(wrds_username='lmota')

    # Erroneous dates: on these dates there are only 1 or 22 (2012-10-29) stock observations, all missing returns
    # seem to be erroneous holidays/weekenddays
    err_dates = pd.to_datetime(['1952-09-27', '1962-08-12', '1972-12-25', '1972-12-28',
                 '1973-01-25', '1973-09-09', '1976-08-29', '1977-01-15', '1977-07-14',
                 '1978-11-26', '1981-11-01', '1985-09-27', '1987-04-19', '1992-10-24',
                 '1995-09-30', '1996-02-19', '2001-09-11', '2012-10-29', '2017-09-30'])

    # Create dist_prc variable - count number of valid days since last
    sql = "SELECT DISTINCT date FROM crspq.dsf"
    dates = db.raw_sql(sql, date_cols=['date'])
    dates = dates.sort_values(['date']).reset_index().date

    # Notice that we are marking weekend as holidays because in the begging of the sample saturdays were trading days.
    all_days = pd.date_range(min(dates), max(dates))
    holidays = all_days[np.logical_not(all_days.isin(dates))]
    holidays = holidays.append(err_dates)
    holidays = holidays.unique()

    # Define CRSP's trading days calendar
    tdays = np.busdaycalendar(weekmask=[1, 1, 1, 1, 1, 1, 1], holidays=holidays.tolist())

    # Test
    # np.busday_count(min(dates), max(dates), busdaycal=TDays) == len(dates) - 1

    return tdays

