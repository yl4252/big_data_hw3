"""
Get breakpoint data from Ken French database.

More detail on pandas_datareader can be founf here: https://pandas-datareader.readthedocs.io/en/latest/index.html

"""
import pandas_datareader.data as web
import datetime
from pandas_datareader.famafrench import get_available_datasets

# %% Main function

ff_breakpoint_var='BE-ME'
def kf_breakpoints(ff_breakpoint_var):
    """
    Query Fama-French breakpoints data.

    Parameters
    ----------
    ff_breakpoint_var: string
        Name of breakpoint variable. Must be in ['ME', 'BE-ME', 'OP', 'INV', 'E-P', 'CF-P', 'D-P', 'Prior_2-12'].

    Examples
    --------
    df = kf_breakpoints(ff_breakpoint_var='Prior_2-12')
    """
    assert ff_breakpoint_var in ['ME', 'BE-ME', 'OP', 'INV', 'E-P', 'CF-P', 'D-P', 'Prior_2-12'], \
        "ff_breakpoint_var must be 'ME', 'BE-ME', 'OP', 'INV', 'E-P', 'CF-P', 'D-P', or 'Prior_2-12'"

    df = web.DataReader(ff_breakpoint_var + '_Breakpoints', 'famafrench', start=datetime.datetime(1925, 1, 1))
    df = df[0].reset_index()

    # Fix messed up column names for momentum breakpoints.
    if ff_breakpoint_var == 'Prior_2-12':
        df.columns = [df.columns[0]] + ['Count'] + df.columns[3:].tolist() + ['empty']
        df.drop('empty', axis=1, inplace=True)

    df.rename(columns={'Date': 'date', 'Count': 'count', '<=0': 'count_<=0', '>0': 'count_>0'}, inplace=True)

    try:
        df['date'] = df['date'].dt.to_timestamp(how='end')
    except AttributeError:
        pass

    # Change breakpoint column names. Calling .tolist() makes column names like (0, 5) tuples.
    for i, col in enumerate(df.columns.tolist()):
        if isinstance(col, tuple):
            df.columns.values[i] = 'p' + str(col[1])


    return df
