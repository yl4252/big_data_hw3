#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2019-02
Code:
    Import BE data from Ken French library.
"""

import pandas as pd

import requests
import tempfile
import zipfile


def dff_be():
    """
    Query Davis-Fama-French Historical Book Equity Data. For data definitions, see
    http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/Data_Library/det_historical_be_data.html

    Column 'be_year' refers to the edition of the Moody's manual from which the book equity observation was collected.
    For example, 1926 edition numbers would be publicly available by June 30, 1926.

    Book equity is in millions of USD.

    First available year is 1926.

    Last available year is 1998.

    """

    raw = requests.get('http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/Historical_BE_Data.zip').content

    with tempfile.TemporaryFile() as tmpf:
        tmpf.write(raw)

        with zipfile.ZipFile(tmpf, 'r') as zf:
            df = pd.read_csv(zf.open('DFF_BE_With_Nonindust.txt'),
                               delim_whitespace=True,
                               names=['permno', 'start_year', 'end_year'] + list(range(1926, 2030)))

    df = df.drop(['start_year', 'end_year'], axis=1)
    df = pd.melt(df, id_vars=['permno'], var_name='rankyear', value_name='be')
    df['rankyear'] = df['rankyear'].astype(int)

    df = df[~df['be'].isin([-99.99, -999])]
    df = df[df['be'].notnull()]

    return df
