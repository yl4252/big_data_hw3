#!/usr/bin/env /shared/share_dff/anaconda3/bin/python
# coding: utf-8

""""
# * Program          : hport_loading_returns.py
# * Function         : calculates high minus low loading portfolio returns.
# * Author           : Kent Daniel, Lira Mota, Simon Rottke and Tano Santos
# * Date             : 2020-01
# * Description      : calculates DMRS long-short portfolios returns.
# * Dependencies     : loading_sorted_portfolio_returns
# * Output Tables    : hport
#
# ## Detailed Description:
# Given loading-sorted portfolio returns, calculates hedge portfolio (long-short) returns
# All returns saved are EXCESS returns.
#
"""

# %% Packages
import pandas as pd
import numpy as np
idx = pd.IndexSlice

import replications.risk_and_returns.loading_sorted_portfolio_returns as ls_returns

desired_width = 320
pd.set_option('display.width', desired_width)
pd.set_option('display.max_columns', 20)


def main(user):
    print('Create hport returns have started.')

    # %% Parameters
    # Long-Short Portfolios
    # ---------------------
    # * Determines the long (first entry of the list) short (second entry of the list) portfolio.
    # * For each char in chars the dictionary names determines the name of the long-short portfolio.
    chars = {'MktRF': {'b': ['b3', 'b1']},
             'SMB': {'s': ['s3', 's1']},
             'HML': {'h': ['h3', 'h1']},
             'RMW': {'r': ['r3', 'r1']},
             'CMA': {'c': ['c3', 'c1']}
             }
    
    # %% Calculate loading-sorted portfolio returns
    ret = ls_returns.main(user=user)
    
    # %% Calculate hedge portfolios
    hport = None
    for char in chars.keys():
        hportname = [*chars[char]][0]
        hportret = ret[char].loc[:, idx[:, :, chars[char][hportname][0]]].mean(axis=1) - \
                 ret[char].loc[:, idx[:, :, chars[char][hportname][1]]].mean(axis=1)
        hportret = hportret.to_frame(hportname)
        if hport is None:
            hport = hportret
        else:
            hport = hport.join(hportret)
        del hportret

    return hport


if __name__ == "__main__":
    user = 'Lira'
    main(user)
